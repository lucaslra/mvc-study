﻿using PartyInvites.Models;
using System.Web.Mvc;

namespace PartyInvites.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ViewResult Index()
        {
            return View();
        }

        [HttpGet]
        public ViewResult RsvpForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult RsvpForm(GuestResponse guestResponse)
        {
            return ModelState.IsValid ? View("Thanks", guestResponse) : View();
        }
    }
}