﻿using System.Web.Optimization;

namespace PartyInvites.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/BootstrapStyles").Include("~/Content/bootstrap.css")
                                                                    .Include("~/Content/bootstrap-theme.css"));

            bundles.Add(new StyleBundle("~/Content/Styles").Include("~/Content/Styles.css"));

            bundles.Add(new ScriptBundle("~/Scripts/jQuery").Include("~/Scripts/jquery-1.9.0.js"));
            bundles.Add(new ScriptBundle("~/Scripts/Bootstrap").Include("~/Scripts/bootstrap.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}