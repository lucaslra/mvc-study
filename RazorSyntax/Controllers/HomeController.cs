﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RazorSyntax.Models;

namespace RazorSyntax.Controllers
{
    public class HomeController : Controller
    {
        private Product product = new Product
                {
                    ProductId = 1,
                    Name = "Kayak",
                    Description = "A boat for one Person",
                    Category = "Watersports",
                    Price = 275M
                };
        // GET: Home
        public ActionResult Index()
        {
            return View(product);
        }

        public ActionResult NameAndPrice()
        {
            return View(product);
        }

        public ActionResult DemoExpression()
        {
            ViewBag.ProductCount = 1;
            ViewBag.ExpressShip = true;
            ViewBag.ApplyDiscount = false;
            ViewBag.Supplier = null;
            return View(product);
        }
    }
}