﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace LanguageFeatures.Models
{
    public static class ExtensionMethods
    {
        public static decimal TotalPrices(this IEnumerable<Product> cart)
        {
            return cart.Sum(p => p.Price);
        }

        public static IEnumerable<Product> FilterByCategory(this IEnumerable<Product> cart, string category)
        {
            return cart.Where(product => product.Category == category);
        }

        public static IEnumerable<Product> Filter(this  IEnumerable<Product> productEnum,
            Func<Product, bool> selectorParam)
        {
            return productEnum.Where(selectorParam);
        }
    }
}