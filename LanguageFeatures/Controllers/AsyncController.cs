﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LanguageFeatures.Controllers
{
    public class AsyncController : Controller
    {
        // GET: Async
        public string Index()
        {
            return "Navigate to a URL to show an example";
        }
    }
}