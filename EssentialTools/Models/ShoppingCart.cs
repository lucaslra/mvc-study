﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EssentialTools.Models.Interfaces;

namespace EssentialTools.Models
{
    public class ShoppingCart
    {
        private readonly IValueCalculator _calc;

        public ShoppingCart(IValueCalculator calc)
        {
            _calc = calc;
        }
        public IEnumerable<Product> Products { get; set; }

        public decimal CalculateProductTotal()
        {
            return _calc.ValueProducts(Products);
        }
    }
}