﻿using System;
using EssentialTools.Models.Interfaces;

namespace EssentialTools.Models
{
    public class MinimumDiscountHelper : IDiscountHelper
    {
        public decimal ApplyDiscount(decimal totalParam)
        {
            if (totalParam > 100m)
                return totalParam * 0.9m;
            if (totalParam >= 10)
                return totalParam - 5m;
            if (totalParam < 10 && totalParam >= 0)
                return totalParam;
            if (totalParam < 0)
                throw new ArgumentOutOfRangeException("totalParam");
            return totalParam;
        }
    }
}