﻿using System.Collections.Generic;
using System.Linq;
using EssentialTools.Models.Interfaces;

namespace EssentialTools.Models
{
    public class LinqValueCalculator : IValueCalculator
    {
        private readonly IDiscountHelper _discounter;
        private static int _counter;

        public LinqValueCalculator(IDiscountHelper discounter)
        {
            _discounter = discounter;

            System.Diagnostics.Debug.WriteLine("Instance {0} created", ++_counter);
        }

        public decimal ValueProducts(IEnumerable<Product> products)
        {
            return _discounter.ApplyDiscount(products.Sum(p => p.Price));
        }
    }
}