﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EssentialTools.Models.Interfaces;

namespace EssentialTools.Models
{
    public class DefaultDiscountHelper : IDiscountHelper
    {
        private decimal _discountSize;

        public DefaultDiscountHelper(decimal discountSize)
        {
            _discountSize = discountSize;
        }
        public decimal ApplyDiscount(decimal totalParam)
        {
            return (totalParam - (_discountSize / 100m * totalParam));
        }
    }
}